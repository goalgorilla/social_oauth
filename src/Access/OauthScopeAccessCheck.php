<?php

namespace Drupal\social_oauth\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\oauth2_server\OAuth2HelperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on OAuth scopes.
 *
 * You can specify the '_oauth_scope' key on route requirements. If you specify
 * a single scope, applications with that scope with have access. If you specify
 * multiple ones you can conjunct them with AND by using a "," and with OR by
 * using "+".
 */
class OauthScopeAccessCheck implements AccessInterface {

  /**
   * The OAuth2 Helper service.
   */
  protected OAuth2HelperInterface $oauth2Helper;

  /**
   * OauthScopeAccessCheck constructor.
   *
   * @param \Drupal\oauth2_server\OAuth2HelperInterface $oauth2_helper
   *   The OAuth2 Helper Service.
   */
  public function __construct(OAuth2HelperInterface $oauth2_helper) {
    $this->oauth2Helper = $oauth2_helper;
  }

  /**
   * Checks access.
   *
   * TODO: Caching in this method relies on the request, there doesn't seem to
   *   be good per-token caching. If a request isn't cacheable this defaults to
   *   no-caching.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, Request $request) {
    // The logic below is borrowed from \Drupal\user\Access\RoleAccessCheck.
    // Requirements just allow strings, so this might be a comma separated list.
    $scope_string = $route->getRequirement('_oauth_scope');

    if ($scope_string) {
      $explode_and = array_filter(array_map('trim', explode(',', $scope_string)));
      if (count($explode_and) > 1) {
        $allowed_scopes = $this->oauth2Helper->getAllowedScopesFromRequest($request);
        $diff = array_diff($explode_and, $allowed_scopes);
        if (empty($diff)) {
          return AccessResult::allowed()->addCacheableDependency($request);
        }
      }
      else {
        $explode_or = array_filter(array_map('trim', explode('+', $scope_string)));
        $allowed_scopes = $this->oauth2Helper->getAllowedScopesFromRequest($request);
        $intersection = array_intersect($explode_or, $allowed_scopes);
        if (!empty($intersection)) {
          return AccessResult::allowed()->addCacheableDependency($request);
        }
      }
    }

    // If there is no allowed role, give other access checks a chance.
    return AccessResult::neutral()->addCacheableDependency($request);
  }

}
