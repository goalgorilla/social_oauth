# Changelog

## 0.1.0 - Initial Version

This version of the module implements an OAuth server for the Open Social app
to authenticate users with. It does not implement any OAauth scopes or
authorization mechanisms.
