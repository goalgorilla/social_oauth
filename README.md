# Social OAuth

The Social OAuth module provides OAuth2 and OpenID Connect functionality for
Open Social to external applications.

<!-- This module is required for the Open Social app to authenticate with Open Social
and for consumers of the GraphQL API to gain access. -->

## Module Status

The current implementation of the module only enables the most basic
functionality for the Open Social App and does not yet provide the authorization
management needed for the GraphQL API.

## Installer Options

This module should not be installed without an extending module such as
social_app or social_graphql that uses the authentication mechanism to provide
access to certain resources. Therefor this module intentionally omits the
`social_oauth.installer_options.yml` file.
