# $Id$
#
# English translation of social_oauth
# Copyright 2021 Open Social <devel@getopensocial.com>
# Generated from files:
#  social_oauth.info.yml: n/a
#  config/install/oauth2_server.server.open_social.yml: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: social_oauth\n"
"POT-Creation-Date: 2022-05-12 00:21+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/install/oauth2_server.server.open_social.yml:0
msgid "Open Social"
msgstr ""

#: social_oauth.info.yml:0
msgid ""
"Provides OAuth2 and OpenID Connect functionality for external applications."
msgstr ""

#: social_oauth.info.yml:0
msgid "Social (Experimental)"
msgstr ""

#: social_oauth.info.yml:0
msgid "Social OAuth"
msgstr ""
